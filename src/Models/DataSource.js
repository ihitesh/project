function randomData() {
    var name = ""
    var nameStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var sexStr = 'MF'
    var isActiveStr = ['True', 'False'];
    var age = Math.floor(Math.random() * 40) + 1
    var isActive = isActiveStr[Math.floor(Math.random() * isActiveStr.length)];
    var sex = sexStr.charAt(Math.floor(Math.random() * sexStr.length));
    for (var i = 0; i < 6; i++)
        name += nameStr.charAt(Math.floor(Math.random() * nameStr.length));
    return [name, age, sex, isActive]
}

const tableData = {
    totalData: [],
    sampleData: [],
    startingRow: 1,
    rowsToShow: 10,
    maxRowsOnPage: 1000,
    outOfRows: 1000000,
    resizeTablePadding : 0,
    getData() {
        let tableOutOfRows = this.totalData.length;
        if (this.outOfRows < this.totalData.length) {
            tableOutOfRows = this.outOfRows
        }
        this.sampleData = this.totalData.slice(0, tableOutOfRows);
    },
    createData() {
        for (let i = 0; i < this.outOfRows; i++) {
            this.totalData.push(randomData())
        }
        this.getData()
    }
}

export default tableData