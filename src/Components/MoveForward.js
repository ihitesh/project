import m from "mithril";

const MoveForward = {
    view: (vnode) => {
        return (
            <span class="bg-blue pa1 br3 br--right white pr2 mv3 fr">
                <span class="mr2 pointer" id="fwd1R" onclick={vnode.attrs.changePageOfTable}> &gt; </span>
                <span class="mr2 pointer" id="fwd1P"onclick={vnode.attrs.changePageOfTable}> &gt;&gt; </span>
                <span class="br pointer" id="fwdLP" onclick={vnode.attrs.changePageOfTable} style="padding-right: 2px;" > &gt;</span>
            </span>
        )
    }
}

export default MoveForward;
