import m from "mithril";

const MoveBackward = {
    view: (vnode) => {
        return (
            <span class="bg-blue pa1 br3 br--left white pl2 fl mv3">
                <span class="bl mr2 pointer" id="bckLP" onclick={vnode.attrs.changePageOfTable} style="padding-left: 2px;"> &lt;</span>
                <span class="mr2 pointer" id="bck1P" onclick={vnode.attrs.changePageOfTable}> &lt;&lt; </span>
                <span class="pointer" id="bck1R" onclick={vnode.attrs.changePageOfTable}> &lt; </span>
            </span>
        )
    }
}

export default MoveBackward;
