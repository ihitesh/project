import m from 'mithril';

const Table = {
    view: (vnode) => {

        let tableRows = []
        for (var i = vnode.attrs.startingRow; i <= vnode.attrs.outOfRows; i++) {
            const value = vnode.attrs.sampleData[i - 1];

            if (tableRows.length < vnode.attrs.rowsToShow && tableRows.length < vnode.attrs.maxRowsOnPage) {
                tableRows.push(<tr>
                    <td class="ph2 ba">{i}</td>
                    <td class="ph2 ba">{value[0]}</td>
                    <td class="ph2 ba">{value[1]}</td>
                    <td class="ph2 ba">{value[2]}</td>
                    <td class="ph2 ba">{value[3]}</td>
                </tr>)
            }
            else
                break;
        }

        const tableHeader =
            (
                <tr>
                    <th class="ba"></th>
                    <th class="ba">Name</th>
                    <th class="ba">Age</th>
                    <th class="ba">Gender</th>
                    <th class="ba">Active</th>
                </tr>
            )
        return (
            <div style="position:relative">
                <div class="center w-70 mw8 mt3" style="min-width:470px">
                    <div style={{'padding-right' : vnode.attrs.resizeTablePadding + "px" }}>
                        <table class="w-100 collapse ma"  >
                            {tableHeader}
                        </table>
                    </div>
                    <div style="max-height: 508px;overflow: auto;" id="tbl-content" class="bb">
                        <table class="w-100 collapse ma ">
                            {tableRows}
                        </table>
                    </div>

                </div>
            </div>
        )
    }
}

export default Table