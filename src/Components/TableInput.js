import m from "mithril";

const TableInput = {
    view: (vnode) => {
        return (
            <span class="dib">
                <span class="ma3 dib">
                    Showing &nbsp;
            <input id="showRows" class='ba tc pa1' type='text' value={vnode.attrs.rowsToShow} onchange={vnode.attrs.updateTable} />
                </span>
                <span class="ma3 dib">
                    Rows out of &nbsp;
            <input id="outOfRows" class='ba tc pa1' type='text' value={vnode.attrs.outOfRows} onchange={vnode.attrs.updateTable} />
                </span>
                <span class="ma3 dib">
                    Starting at row &nbsp;
            <input id="startingRow" class='ba tc pa1 ' type='text' value={vnode.attrs.startingRow} onchange={vnode.attrs.updateTable} />
                </span>
            </span>
        )
    }
}

export default TableInput;
