import m from 'mithril'

import DataSource from '../Models/DataSource'
import MoveForward from "./MoveForward";
import MoveBackward from "./MoveBackward";
import TableInput from "./TableInput";
import Table from "./Table";


var { totalData, sampleData, startingRow, rowsToShow, maxRowsOnPage, outOfRows } = DataSource

function updateTable(event) {
    event.preventDefault();
    if (event.target.value == '') {
        event.target.value = 0
    }
    if (event.target.id == 'showRows') {
        DataSource.rowsToShow = parseInt(event.target.value);
    }
    else if (event.target.id == 'outOfRows') {
        DataSource.outOfRows = parseInt(event.target.value);
        DataSource.getData()
        return;
    }
    else if (event.target.id == 'startingRow') {
        DataSource.startingRow = parseInt(event.target.value);
    }
    if (DataSource.rowsToShow > DataSource.maxRowsOnPage) {
        return;
    }
}

function changePageOfTable() {
    const id = this.id;

    if (id == 'fwd1R') {
        if (DataSource.startingRow >= DataSource.outOfRows) {
            return;
            // If we want to start the table from initial row when user is on last page and presses next data button (All comment tags describe the same functionality)
            // DataSource.startingRow = 1;
        } else {
            DataSource.startingRow++;
        }
    }
    else if (id == 'fwd1P') {
        if (DataSource.startingRow + DataSource.rowsToShow > DataSource.outOfRows) {
            return;
        // DataSource.startingRow = 1;
           
        } else {
            DataSource.startingRow = DataSource.startingRow + DataSource.rowsToShow;
        }
    }
    else if (id == 'fwdLP') {
        DataSource.startingRow = DataSource.outOfRows - DataSource.rowsToShow + 1;
    }
    else if (id == 'bck1R') {
        if (DataSource.startingRow === 1) {
            return;
            // DataSource.startingRow = DataSource.outOfRows;
        }
        DataSource.startingRow--
    }
    else if (id == 'bck1P') {
        if (DataSource.startingRow === 1 || DataSource.startingRow <= DataSource.rowsToShow) {
            return;
            // DataSource.startingRow = DataSource.outOfRows + 1;
        };
        DataSource.startingRow = DataSource.startingRow - DataSource.rowsToShow
    }
    else if (id == 'bckLP') {
        DataSource.startingRow = 1;
    }

}


const App = {

    oninit: DataSource.createData(),

    onupdate: function (vnode) {
        var element = document.getElementById("tbl-content");
        if (element) {
            var diffWidth = element.offsetWidth - element.querySelector("table").offsetWidth;
            if (diffWidth != DataSource.resizeTablePadding) {
                DataSource.resizeTablePadding = diffWidth
                m.redraw()
            }
        }
    },

    view: () => {
        const tableControls =
            (
                <div class="tc pt2 pb3 center" style="min-width: 680px;max-width: 850px;">
                    <MoveBackward changePageOfTable={changePageOfTable} />
                    <MoveForward changePageOfTable={changePageOfTable} />
                    <TableInput
                        updateTable={updateTable}
                        startingRow={DataSource.startingRow}
                        outOfRows={DataSource.outOfRows}
                        rowsToShow={DataSource.rowsToShow} />
                </div>
            )

        let table;

        if ((DataSource.startingRow > DataSource.outOfRows) || DataSource.rowsToShow == 0 || DataSource.startingRow == 0) {
            table = (<div class="ma4">
                {tableControls}
                <div class="tc f4 mv4">Please enter relevant value. No table to show</div>
            </div>
            )
        }
        else {
            table =
                (<div class="ma4">
                    {tableControls}
                    <Table
                        startingRow={DataSource.startingRow}
                        outOfRows={DataSource.outOfRows}
                        rowsToShow={DataSource.rowsToShow}
                        sampleData={DataSource.sampleData}
                        resizeTablePadding={DataSource.resizeTablePadding}
                        maxRowsOnPage={DataSource.maxRowsOnPage} />
                </div>)
        }

        return (
            table
        )
    }
}

export default App;